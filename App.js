/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import { Avatar } from 'react-native-elements';
import { Image } from 'react-native-elements';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const ICONSIZE = 40;

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.khungvien1}>
          <Text style={styles.welcome}>Settings</Text>
        
        </View>
        <View style={styles.khungvien2}>
          <Avatar size='xlarge' rounded source={{uri:'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',}}/>     
          <View style = {styles.circle}>
            <Icon type = 'ion-icon' name = 'md-brush' style={{top: 20}} size = {ICONSIZE-10}></Icon>
          </View>
          <Text style = {{color: 'black', top: 29}}>chuducanhchy</Text>
        </View>
        <View style={styles.khungvien3}>
          <Text style={styles.text}>Hinge Member.</Text>

        
        </View>
        <View style={styles.khungvien4}>
          <View style={styles.lef}>
            <Text style = {styles.textlef} >Prefernces</Text>
          </View>
          <View style={styles.rig}>
            <Icon type = 'ion-icon' name = 'md-switch' style={styles.icon} size = {ICONSIZE}></Icon>
          </View>
        </View>
        <View style={styles.khungvien5}>
          <View style={styles.lef}>
            <Text style = {styles.textlef} >Account</Text>
          </View>
          <View style={styles.rig}>
            <Icon type = 'ion-icon' name = 'md-cog' style={styles.icon} size = {ICONSIZE}></Icon>
          </View>
        </View>
        <View style={styles.khungvien6}>
          <View style={styles.lef}>
            <Text style = {styles.textlef} >Help Center</Text>
          </View>
          <View style={styles.rig}>
            <Icon type = 'ion-icon' name = 'md-help' style={styles.icon} size = {ICONSIZE}></Icon>
          </View>
        </View>
        <View style={styles.khungvien7}>
          <View style={styles.lef}>
            <Text style = {styles.textlef} >Ask an Expert</Text>
          </View>
          <View style={styles.rig}>
            <Icon type = 'ion-icon' name = 'md-color-wand' style={styles.icon} size = {ICONSIZE}></Icon>
          </View>       
        </View>
        <View style={styles.khungvien8}>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   // justifyContent: 'center',
  //  alignItems: 'center',
    backgroundColor: 'white',
  },
  khungvien1:{
    flex:0.8,
    justifyContent:'flex-end',
    borderWidth:1,
    borderColor:'#eee',
  },
  khungvien2:{
    flex:2.2,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:'#eee',
  },
  khungvien3:{
    flex:0.75,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth:1,
    borderColor:'#eee',
  },
  khungvien4:{
    flex:0.5,
    justifyContent:'flex-end',
    borderWidth:1,
    flexDirection: 'row',
    borderColor:'#eee',
  },
  khungvien5:{
    flex:0.5,
    flexDirection: 'row',
    justifyContent:'flex-end',
    borderWidth:1,
    borderColor:'#eee',
  },
  khungvien6:{
    flex:0.5,
    flexDirection: 'row',
    justifyContent:'flex-end',
    borderWidth:1,
    borderColor:'#eee',
  },
  khungvien7:{
    flexDirection: 'row',
    flex:0.5,
    justifyContent:'flex-end',
    borderWidth:1,
    borderColor:'#eee',
  },
  khungvien8:{
    flex:0.75,
    justifyContent:'flex-end',
    borderWidth:1,
    borderColor:'#eee',
  },
  welcome: {
    borderColor: 'black',
    fontSize: 25,
    color:'black',
    justifyContent:'flex-end',
    margin: 10,
  },
  text:{
    color:'black'
  },
  lef:{
    flex:4,
    alignItems:'flex-start'
  },
  rig:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',

  },
  textlef: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily:'Times New Roman',
    color:'black',
  },
  circle: {
    position: 'absolute',
    width: 100,
    height: 100,
    borderRadius: 100/2,
    backgroundColor: 'white',
    top: 150,
    alignItems: 'center',
  }
});